#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

pushd /home/pi
pushd /home/pi/sensor-checker

apt -y install python-pip
pip install evdev
echo "Dependencies Installed Successfully!"

mkdir /opt/sensor-checker
mv ./src/* /opt/sensor-checker

popd

cd /opt
wget abyz.me.uk/rpi/pigpio/pigpio.zip
unzip pigpio.zip
cd PIGPIO
make
make install

popd

rm -rf sensor-checker

sed -i '$i sudo bash \/opt\/sensor-checker\/launch.sh' /etc/rc.local
reboot