import os
import json
from traceback import format_exc
from argparse import ArgumentParser
from container import Container

def launchApplication(description, main):
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)

    parser = ArgumentParser(description=description)
    parser.add_argument('--config',
        dest='configFile',
        help="Configuraton file name",
        default='config.json',
        nargs='?'
    )
    parser.add_argument('--bg',
        dest='background',
        help="Run the program in the background",
        action='store_true'
    )

    args = parser.parse_args()

    cfg = json.load(open(args.configFile))

    if args.background:
        pid = os.fork()
        if pid:
            pidFile = open(cfg['pidFile'], 'w')
            pidFile.write(str(pid))
            pidFile.close()
            exit(0)

    if os.path.isfile('boxid'):
        with open('boxid', 'r') as fid:
            cfg['Http']['boxid']=fid.read().replace('\n', '')

    container = Container(cfg, args)
    logger = container.logger

    try:
        main(container)
    except Exception as e:
        try:
            if hasattr(container, 'transport'):
                container.transport.sendData('PANIC', str(e))
        finally:
            logger.error('UNEXPECTED EXCEPTION')
            logger.error(format_exc(e))
            logger.error('Now cracks a noble heart. Good night sweet prince.')
            os.kill(os.getpid(), 9)
