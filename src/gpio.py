import RPi.GPIO as Connection

class GPIO:
    def __init__(self, pin, logger):
        self.pin = pin

        Connection.setmode(Connection.BCM)  # Broadcom pin-numbering scheme
        Connection.setup(self.pin, Connection.OUT)

        logger.info('GPIO pin {} set'.format(pin))


    def setValue(self, v):
        if v != 1 and v != 0:
            raise Exception('Incorrect value for GPIO pin: {}. 1 or 0 is expected.'.format(v))

        if v == 1:
            v = Connection.HIGH
        else:
            v = Connection.LOW

        Connection.output(self.pin, v)


def GPIOFactory(logger):
    return lambda pin: GPIO(pin, logger)
