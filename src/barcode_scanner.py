from threading import Thread
from multiprocessing import Queue
from evdev import InputDevice, ecodes

import time

class BarcodeScanner:
    hid = {35: 'h', 20: 't', 25: 'p', 31: 's', 42: '', 39: ';', 53: '/', 17: 'w', 49: 'n', 52: '.', 22: 'u', 30: 'A',
           48: 'B', 46: 'C', 32: 'D', 18: 'E', 33: 'F', 11: '0', 2: '1', 3: '2', 4: '3', 5: '4', 6: '5', 7: '6', 8: '7',
           9: '8', 10: '9', 82: '0', 79: '1', 80: '2', 81: '3', 75: '4', 76: '5', 77: '6', 71: '7', 72: '8', 73: '9'}

    alt = {104: 'h', 116: 't', 112: 'p', 115: 's', 119: 'w', 101: 'e', 110: 'n', 46: '.', 47: '/', 58: ':', 117: 'u',
           65: 'A', 66: 'B', 67: 'C', 68: 'D', 69: 'E', 70: 'F', 48: '0', 49: '1', 50: '2', 51: '3', 52: '4', 53: '5', 54: '6',
           55: '7', 56: '8', 57: '9'}

    def __init__(self, logger):
        self.logger = logger
        self.queue = Queue()

        self.__connectScanner()

        self.logger.info('Barcode scanner setup')

    def run(self):
        thread = Thread(target=self.__run, args=(self.queue, ))
        thread.start()

        self.logger.info('Barcode watcher thread started')

    def __run(self, queue):
        done = False
        serial = ""
        altDown = False
        altNum = 0

        for event in self.fp.read_loop():
            if done:
                serial = int(serial[-6:], 16)
                queue.put((serial,))
                serial = ""
                done = False

            if event.type == ecodes.EV_KEY:
                # When alt key goes down
                if event.code == 56 and event.value == 1:
                    altDown = True

                # When alt key released
                elif event.code == 56 and event.value == 0:
                    serial += self.alt[altNum]
                    altNum = 0
                    altDown = False

                # Collect alt num
                elif altDown and event.code != 28 and event.value == 0:
                    altNum *= 10
                    altNum += int(self.hid[event.code])

                # Collect other input
                elif not altDown and event.code != 28 and event.value == 0:
                    serial += self.hid[event.code]

                # Enter key to stop collection
                elif event.code == 28 and event.value == 0:
                    done = True

    def __connectScanner(self):
        try:
            self.fp = InputDevice("/dev/input/event0")  # my keyboard
        except IOError:
            self.logger.info('Scanner not Connected')