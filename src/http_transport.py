import httplib
import urllib
import time
from traceback import format_exc
from socket import error as socket_error

class HttpTransport:

    def __init__(self, config, logger):
        self.config = config
        self.logger = logger

        self.logger.info('Box ID: {}'.format(config['boxid']))

    def sendData(self, results, sensorID):
        self.logger.info('Sending HTTP packet')
        if self.config['scheme'] == 'http':
            connection = httplib.HTTPConnection(self.config['host'], self.config['port'])
        else:
            connection = httplib.HTTPSConnection(self.config['host'], self.config['port'])

        postData = urllib.urlencode({
            "boxid": self.config['boxid'],
            "sensorID": sensorID,
            "idTest": results[0](),
            "tempTest": results[1](),
            "vibrTest": results[2]()
        })


        try:
            connection.request('POST', self.config['uri'], postData)
        except socket_error as serr:
            self.logger.error('Socket error')
            self.logger.error(serr)
            return False

        try:
            response = connection.getresponse()
        except httplib.HTTPException as e:
            self.logger.error('Unexpected HTTP exception')
            self.logger.error(format_exc(e))
            return False

        if response.status == 200:
            self.logger.info('HTTP 200 OK received')
            return True
        else:
            self.logger.error(
                'Unexpected HTTP {} {} received'.format(response.status, response.reason)
            )
            return False
