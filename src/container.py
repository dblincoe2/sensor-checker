import logging
from logging.handlers import RotatingFileHandler
from traceback import format_exc

from gpio import GPIOFactory
from serial_wrapper import SerialLive
from sensor_controller import SensorController
from barcode_scanner import BarcodeScanner
from status import Status
from http_transport import HttpTransport

class Container:
    def __init__(self, cfg, args):
        self.configureLogger(cfg['Logger'], args.background)
        self.logger.info('***************** APPLICATION STARTED ******************')
        self.logger.info('Configuration loaded from {}'.format(args.configFile))

        try:
            self.GPIO = GPIOFactory(logging.getLogger('app.gpio'))

            LEDS = self.createLEDS(cfg['Status'])
            self.status = Status(LEDS)

            self.transport = HttpTransport(cfg['Http'], logging.getLogger('app.http'))

            self.ID = BarcodeScanner(logging.getLogger('app.barcode'))

            self.createSensors(cfg, self.ID.queue)

            self.tests = [
                lambda: self.controller.tempTest,
                lambda: self.controller.vibrationTest
            ]

            if cfg['SensorController']['UsageType'] == 'FACTORY':
                self.tests.insert(0, lambda: self.controller.IDTest)
                self.ID.run()

        except Exception as e:
            self.logger.error('UNEXPECTED CONFIGURATION ERROR')
            self.logger.error(format_exc(e))
            raise
        self.logger.info('***************** APPLICATION CONFIGURED ******************')

    def configureLogger(self, cfg, suppressConsole):
        logFormatter = logging.Formatter(
            '%(asctime)s - %(levelname)s - %(name)s - %(message)s'
        )
        rootLogger = logging.getLogger('app')
        rootLogger.propagate = False
        rootLogger.setLevel(cfg['level'])
        fileHandler = RotatingFileHandler(
            cfg['file'],
            maxBytes= 25 * 1024 * 1024,
            backupCount = 5
        )
        fileHandler.setFormatter(logFormatter)
        rootLogger.addHandler(fileHandler)

        if not suppressConsole:
            consoleHandler = logging.StreamHandler()
            consoleHandler.setFormatter(logFormatter)
            rootLogger.addHandler(consoleHandler)

        self.logger = rootLogger

    def createSensors(self, cfg, IDQueue):
        serial = SerialLive(logging.getLogger('app.serial'), cfg['Sensor']['tx'], cfg['Sensor']['rx'], cfg['Sensor']['baud'])

        self.controller = SensorController(serial, logging.getLogger('app.sensor'), cfg['SensorController'], IDQueue)

    def createLEDS(self, cfg):
        LEDS = []
        for led in range(3):
            LEDS.append(self.GPIO(cfg['LED' + str(led + 1)]['redPin']))
            LEDS.append(self.GPIO(cfg['LED' + str(led + 1)]['greenPin']))
            LEDS.append(self.GPIO(cfg['LED' + str(led + 1)]['bluePin']))
        return LEDS