class Status:

    def __init__(self, LEDS):
        self.LEDS = LEDS

    def waiting(self):
        self.clear()
        for idx in range(3):
            self.LEDS[idx * 3 + 2].setValue(0)

    def reporting(self, results):
        self.clear()
        for idx, result in enumerate(results):
            if result is None:
                self.LEDS[3 * idx + 1].setValue(0)
                self.LEDS[3 * idx].setValue(0)
            elif result() is True:
                self.LEDS[3 * idx + 1].setValue(0)
                self.LEDS[3 * idx].setValue(1)
            elif result() is False:
                self.LEDS[3 * idx].setValue(0)
                self.LEDS[3 * idx + 1].setValue(1)

    def clear(self):
        for idx in range(9):
            self.LEDS[idx].setValue(1)