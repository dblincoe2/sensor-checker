from time import sleep
from crc import CRC
import pigpio

class SerialLive:
    def __init__(self, logger, tx, rx, baudrate=33333):
        self.logger = logger
        self.tx = tx
        self.rx = rx
        self.baudrate = baudrate

        self.logger.info('Serial setup. TX: {}, RX: {}'.format(self.tx, self.rx))
        self.connection = pigpio.pi()
        self.connection.set_mode(self.tx, pigpio.OUTPUT)

        pigpio.exceptions = False
        self.connection.bb_serial_read_close(self.rx)
        pigpio.exceptions = True
        self.open = False

        self.connection.bb_serial_read_open(self.rx, self.baudrate, 8)

        self.connection.wave_clear()


    def read(self, numBits=None):
        count = 1
        data = []

        sleep(.2)

        while count > 0:
            (count, chunk) = self.connection.bb_serial_read(self.rx)
            if count:
                data.extend(list(chunk))
            elif count < 0:
                return None
            sleep(.02)

        if numBits is None or numBits == len(data):
            return data
        elif len(data) == 0:
            self.logger.error("It seems like a sensor isn't attached")
            return None
        else:
            self.logger.error("Serial read failed. The number of bits expected differed from the received number.")
            self.logger.error('Expected Length: {}. Actual Length: {}'.format(numBits, len(data)))
            return None

    def write(self, data):
        self.connection.wave_clear()

        self.connection.wave_add_serial(self.tx, self.baudrate, data)
        wav = self.connection.wave_create()
        self.connection.wave_send_using_mode(wav, pigpio.WAVE_MODE_ONE_SHOT_SYNC)

        self.connection.wave_delete(wav)

    def expectData(self, timeout):
        sleep(timeout)
        responseBit = self.read()

        if responseBit is not None and len(responseBit) is not 0:
            self.logger.info("The response bit was received.")
            return responseBit
        else:
            self.logger.error("The response bit was not received.")
            return None



    def processBufferData(self, rawBuffer, startBit):
        try:
            dataCrc = (rawBuffer[-2:-1][0] << 8) + rawBuffer[-1:][0]
        except (TypeError, IndexError):
            return None

        rawBuffer = rawBuffer[startBit:-2]

        crc = CRC()

        for sample in range(len(rawBuffer)):
            crc.update(rawBuffer[sample])

        if crc.get() != dataCrc:
            self.logger.error('CRC do not match. The data is most likely corrupted.')
            return None

        return rawBuffer

    def resetInputBuffer(self):
        count = 1
        while count:
            (count, chunk) = self.connection.bb_serial_read(self.rx)

    def resetOutputBuffer(self):
        self.connection.wave_clear()

