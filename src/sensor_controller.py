from sensor_factory import SensorFactory
from threading import Thread
import time

class SensorController:
    def __init__(self, serial, logger, cfg, IDQueue):
        self.sensorFactory = SensorFactory(serial, logger, cfg)
        self.sensor = None

        self.logger = logger
        self.cfg = cfg
        self.IDQueue = IDQueue
        self.reset()

    def checkForSensor(self):
        self.sensor = self.sensorFactory.updateSensorType()

        if self.sensor is not None:

            # Removes multiple scanned barcodes
            while not self.IDQueue.empty():
                self.IDQueue.get()

            self.ID = self.sensor.readID()

    def runTests(self, barcodeCallbackQueue):

        if self.cfg['UsageType'] == 'FACTORY':
            thread = Thread(target=self.__runBarcode, args=(barcodeCallbackQueue,))
            thread.start()
        else:
            barcodeCallbackQueue.put(('DONE',))

        self.vibrationTest = self.sensor.readVibration()

        self.tempTest = self.sensor.readTemp()

    def __runBarcode(self, queue):
        while True:
            if not self.IDQueue.empty():
                scannedID = self.IDQueue.get()[0]
                self.IDTest = (self.ID == scannedID)
                queue.put(('DONE',))
                break

            time.sleep(0.05)

    def reset(self):
        self.ID = None


        self.IDTest = None
        self.tempTest = False
        self.vibrationTest = False

        self.logger.info('The V1 subsystem has been reset')
