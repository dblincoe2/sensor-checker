from launch import launchApplication
from multiprocessing import Queue
import time

barcodeCallbackQueue = Queue()

def main(c):
    waiting = False

    while True:
        if c.controller.sensor is None and not waiting:
            c.status.clear()
            emptyQueue()
            waiting = True

        elif c.controller.sensor is not None and waiting:
            waiting = False
            c.status.waiting()

            # Report the temp and vibr tests
            c.controller.runTests(barcodeCallbackQueue)
            c.status.reporting(c.tests)

            # Wait till sensor is unplugged or barcode is scanned. Then report ID test
            waitForBarcode(c)
            c.status.reporting(c.tests)

            c.controller.reset()

        c.controller.checkForSensor()

def emptyQueue():
    while not barcodeCallbackQueue.empty():
        barcodeCallbackQueue.get()

def waitForBarcode(c):
    while barcodeCallbackQueue.empty():
        c.controller.checkForSensor()

        if c.controller.sensor is None:
            break

        time.sleep(0.05)

    if not barcodeCallbackQueue.empty():
        barcodeCallbackQueue.get()

if __name__ == "__main__":
    launchApplication(
        "Conveyor sensor - unified module.",
        main
    )
