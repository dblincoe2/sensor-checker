#!/bin/bash
pushd /opt/sensor-checker

CONFIG=config.json

sudo pigpiod
sudo python ./main.py --bg --config $CONFIG
