from crc import CRC
from struct import unpack
from math import sqrt

class SensorV2:

    AXIS = ['x', 'y', 'z']

    def __init__(self, serial, logger, cfg):
        self.logger = logger
        self.serial = serial
        self.cfg = cfg

        self.rawDataAcquired = False


    def resetBuffers(self):
        self.logger.info('Resetting serial I/O buffers')

        self.rawDataAcquired = False

        self.serial.resetInputBuffer()
        self.serial.resetOutputBuffer()

    def readID(self):
        self.serial.write([73])

        buffer = self.serial.read(6)

        if buffer is []:
            return None
        else:
            rawBuffer = self.__processBufferData(buffer, 0)

            if rawBuffer is None:
                self.logger.error('Error getting ID. No data was returned from processing')
                return False

            rawBuffer = bytearray(rawBuffer)
            ID = unpack('>I', rawBuffer)[0]

            return ID

    def readTemp(self):
        self.resetBuffers()
        tempCfg = self.cfg['temperatureTest']

        # Not a mistake. It needs to run twice to get good values. I don't know why. Ask V2 designer
        for x in range(2):
            self.serial.write([84])
            buffer = self.serial.read(4)

        self.logger.info('Reading Sensor Temperature: {}'.format(buffer))


        if buffer is []:
            self.logger.error('Error getting Temp. No data was returned')
            return False

        rawBuffer = self.__processBufferData(buffer, 0)

        if rawBuffer is None:
            return False

        rawBuffer = bytearray(rawBuffer)
        temperature = unpack('>h', rawBuffer)

        temperature = temperature[0] / 256.0

        if temperature <= tempCfg['lowerLimit'] or temperature >= tempCfg['upperLimit']:
            self.logger.error('Temperature reading error. Reading exceeded limits: {}'.format(temperature))
            return False

        self.logger.info('Temperature reading successfully checked. Temperature: {}'.format(temperature))
        return True

    def readVibration(self):
        vibCfg = self.cfg['vibrationTest']

        data = {}
        for axis in self.AXIS:
            self.switchAxis(axis)

            self.triggerData(vibCfg['sampleNum'], vibCfg['samplePeriod'])
            self.waitForAcquire(vibCfg['sampleNum'], vibCfg['samplePeriod'])

            if self.rawDataAcquired:
                data[axis] = self.fetchData(vibCfg['sampleNum'])
                if data[axis] is None:
                    return False
            else:
                return False

        accel = self.__accelComputation(data)
        if accel <= vibCfg['lowerLimit'] or accel >= vibCfg['upperLimit']:
                self.logger.error('Vibration reading error. Reading exceeded limits: {}'.format(data))
                return False
        self.logger.info('Vibration reading successful! G\'s: {}'.format(round(accel, 3)))
        return True

    def triggerData(self, sampleNum, samplePeriod):
        self.resetBuffers()
        self.__aquireBuffer(sampleNum, samplePeriod)

    def waitForAcquire(self, sampleNum, samplePeriod):

        if self.serial.expectData(0) is None:
            self.logger.error('Error: Acquire request not received')
            self.rawDataAcquired = False
            return

        timeout = ((samplePeriod / 10000000.0) * sampleNum) + 0.5

        self.logger.info('Raw: reading, expected time: {}'.format(timeout - 0.5))

        if self.serial.expectData(timeout) is None:
            self.logger.error('Error: No Raw reading to acquire')
            self.rawDataAcquired = False
            return

        self.logger.info('Raw reading acquired')

        self.rawDataAcquired = True

    def fetchData(self, sampleNum):
        self.logger.info('Fetching Data')

        raw = self.__readBufferData(sampleNum)

        rawBuffer = self.__processBufferData(raw, 1)

        if rawBuffer is None:
            return None

        # Convert raw buffer to array with readings
        rawBuffer = bytearray(rawBuffer)
        rawData = unpack('>{}h'.format(len(rawBuffer) / 2), rawBuffer)

        if rawData is None:
            return None
        else:
            return rawData

    def switchAxis(self, axis=None):
        self.axis = axis

        self.logger.debug('Switching to axis "{}"...'.format(self.axis))
        self.serial.write([ord(self.axis)])
        response = self.serial.expectData(0)
        self.logger.debug('Switched axis to {}. Sensor replied {}.'.format(
            self.axis,
            response
        ))

    def __processBufferData(self, rawBuffer, startBit):
        try:
            dataCrc = (rawBuffer[-2:-1][0] << 8) + rawBuffer[-1:][0]
        except TypeError:
            return None

        rawBuffer = rawBuffer[startBit:-2]

        crc = CRC()

        for sample in range(len(rawBuffer)):
            crc.update(rawBuffer[sample])

        if crc.get() != dataCrc:
            self.logger.error('CRC do not match. The data is most likely corrupted.')
            return None

        return rawBuffer


    def __aquireBuffer(self, sampleNum, samplePeriod):
        buffer = []
        buffer.append(65)
        buffer.append(sampleNum >> 8)
        buffer.append(sampleNum & 0xFF)
        buffer.append(samplePeriod >> 8)
        buffer.append(samplePeriod & 0xFF)

        crc = CRC()
        crc.update(sampleNum >> 8)
        crc.update(sampleNum & 0xFF)
        crc.update(samplePeriod >> 8)
        crc.update(samplePeriod & 0xFF)
        crc = crc.get()

        buffer.append(crc >> 8)
        buffer.append(crc & 0xFF)

        self.serial.write(buffer)

    def __readBufferData(self, sampleNum):
        buffer = []
        buffer.append(82)
        buffer.append(sampleNum >> 8)
        buffer.append(sampleNum & 0xFF)

        crc = CRC()
        crc.update(sampleNum >> 8)
        crc.update(sampleNum & 0xFF)
        crc = crc.get()

        buffer.append(crc >> 8)
        buffer.append(crc & 0xFF)

        self.serial.write(buffer)

        return self.serial.read((2 * sampleNum) + 3)

    def __accelComputation(self, accelVector):
        for axis in self.AXIS:
            accelVector[axis] = sum(accelVector[axis]) / (len(accelVector[axis]) * 4096.0)

        return sqrt(accelVector['x'] ** 2 + accelVector['y'] ** 2 + accelVector['z'] ** 2)


