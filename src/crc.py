class CRC:
    def __init__(self):
        self.crc = 0xFFFF

    def update(self, data):

        data ^= self.crc >> 8
        data ^= data << 4
        data &= 0xFF

        self.crc = ((data << 8) | (self.crc >> 8)) ^ (data >> 4) ^ (data << 3)

    def get(self):
        return self.crc


