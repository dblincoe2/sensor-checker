from sensor_v2 import SensorV2


class SensorFactory:
    def __init__(self, serial, logger, cfg):
        self.serial = serial
        self.logger = logger
        self.cfg = cfg

    def updateSensorType(self):
        self.serial.write([81])

        buffer = self.serial.read()

        if buffer is []:
            return None
        else:
            processedData = self.serial.processBufferData(buffer, 0)

            if processedData is None:
                self.logger.error('Error getting ID. No data was returned from processing')
                return None

            sensorClass = SensorV2
            cfg = self.cfg['V2']

        return sensorClass(self.serial, self.logger, cfg)
